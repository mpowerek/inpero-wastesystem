

<?php
use Entities\Kernel\Kernel;
session_start();
// inkluduje composera
include '../vendor/autoload.php';
// pobieram kontroler i czynność
global $kernel;
$kernel = new Kernel();
$controller     = $_GET['controller'] ?? 'Home';
$do             = $_GET['do'] ?? 'Main';
// ładuje klase kontrolera
include '../src/'.$controller.'Controller/'.$controller.'.php';
$classInit=$controller."\\".$controller;
$init = new $classInit();
$init->{$do}();
// ładuje szablon jeżeli istnieje
