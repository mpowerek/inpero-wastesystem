
<?php
if(isset($_SESSION['redirect']))
{
?>
<div>
<div class='system_alert' style="display:none;background: rgba(0, 0, 0, 0.48); color: rgb(255, 255, 255); position: fixed; width: 600px; font-size: 11px; margin-top: 5px; border-radius: 4px; text-align: center; padding: 0px; left: 50%; margin-left: -300px; z-index: 99999;">
<h3 style="color:white;"><?php echo $_SESSION['redirect'] ?></h3>
</div>
</div>
<script>
$(document).ready(function(){
    $('.system_alert').slideDown().delay(3000).slideUp();
});
</script>
    <?php
    $_SESSION['redirect'] = null;
}
?>