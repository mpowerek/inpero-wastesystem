<?php
global $kernel;
// rendering previus page
if($data['actualPage'] > 1)
  $prev = 'index.php?controller='.$kernel->controller.'&page='.($data['actualPage'] - 1);
else
  $prev = '#';

// rendering next page
if($data['actualPage'] < $data['totalPages'])
  $next = 'index.php?controller='.$kernel->controller.'&page='.($data['actualPage'] + 1);
else
  $next = '#';

// show extra links
$actual = 'index.php?controller='.$kernel->controller.'&page='.$data['actualPage'];
$nextLink = 'index.php?controller='.$kernel->controller.'&page='.($data['actualPage'] + 1);
$prevLink = 'index.php?controller='.$kernel->controller.'&page='.($data['actualPage'] - 1);
?>
<div style="float: left; text-align: center; font-size: 10px;">
<?php
echo 'strona '.$data['actualPage'].' z '.$data['totalPages'];
?>
<nav aria-label="Page navigation example">
  <ul class="pagination" style="margin:0;">

  <?php
    if($data['totalPages'] > 1)
    {
      echo ' <li class="page-item"><a class="page-link" href="'.$prev.'">Previous</a></li>';
      if($data['actualPage'] != 1)
      {
      echo '<li class="page-item"><a class="page-link" href="'.$prevLink.'">'.($data['actualPage'] - 1).'</a></li>';
      }
      echo '<li class="page-item"><a class="page-link" href="'.$actual.'">'.$data['actualPage'].'</a></li>';
      echo '<li class="page-item"><a class="page-link" href="'.$nextLink.'">'.($data['actualPage'] + 1).'</a></li>';
      echo '<li class="page-item"><a class="page-link" href="'.$next.'">Next</a></li>';
    }
  ?>
  </ul>
</nav>

</div>