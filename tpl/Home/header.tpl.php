<?php global $kernel;?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Igor Leszczynski">
    <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/favicon.png">
    <title><?php echo $kernel->pagetitle?></title>
    <?php 
    $kernel->loadCSS('Home', 'http://netdna.bootstrapcdn.com/twitter-bootstrap/2.2.2/css/bootstrap-combined.min.css');
    $kernel->loadCSS('Home');
    $kernel->loadCSS('Home', 'https://use.fontawesome.com/releases/v5.3.1/css/all.css');
    $kernel->loadJs('Home');
    ?>
</head>
<body>
    <?php $kernel->theme('redirectAlert', 'Home'); ?>