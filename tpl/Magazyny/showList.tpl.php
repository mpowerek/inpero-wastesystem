<div>
<a href="index.php?controller=Magazyny&do=add" class="btn btn-success">Dodaj magazyn</a>
<br><br>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">id</th>
                <th scope="col">Kod</th>
                <th scope="col">Nazwa</th>
                <th scope="col">Akcja</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($data['warehousesList']['data'] as $r)
            {
                echo '<tr><th scope="row">'.$r['id'].'</th>
                        <td>'.$r['kod'].'</td>
                        <td>'.$r['nazwa'].'</td>
                        <td><a class="btn btn-info" href="index.php?controller=Magazyny&do=details&id='.$r['id'].'">Edytuj</a></td></tr>'; 
            }
            ?>
        </tbody>
    </table>
</div>
<?php
global $kernel;
$kernel->showPagination($data['warehousesList']);

?>