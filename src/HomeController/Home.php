<?php
namespace Home;
use Entities\Kernel\Database\Database;
class Home {
    function __construct() {
        
    }
    function Main() {
        global $kernel;
        // $kernel->db->query('SELECT * FROM `magazyny`');

        // $kernel->theme('test', 'test');
        $kernel->theme('header', 'Home');
        $kernel->theme('body', 'Home');
        $kernel->theme('homeInside', 'Home');
        $kernel->theme('footer', 'Home');
    }
}
?>