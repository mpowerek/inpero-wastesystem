<?php
namespace Entities\Warehouse;
use Entities\Kernel\Database\Database;
class Warehouse extends Database {
    public $db;
    public $id, $kod, $name;
    function __construct($id=NULL)
    {
        $this->db = new Database();
        if($id != null)
        {
            $sth = $this->db->prepare('SELECT * FROM `magazyny` WHERE id=?');
            $sth->execute(Array($id));
            while($row = $sth->fetch())
            {
                $this->id = $row['id'];
                $this->kod = $row['kod'];
                $this->name = $row['nazwa'];
            }
        }
    }
    public function remove($list=array())
    {
            try {
                foreach($list as $r)
                {
                $sth = $this->db->prepare('DELETE FROM `magazyny` WHERE `id` = ?');
                if(!$result = $sth->execute(Array($r)))
                    throw new \Exception('Delete warehouse failed ('.$r.')');
                }
                global $kernel;
                $kernel->redirect('index.php?controller=Magazyny', 'Magazyn usunięty pomyślnie!');
            } catch (\Throwable $th) {
                echo '<br/>Uncaught Exception: ',  $th->getMessage(), "\n";
            }
    }
    public function changeName($new)
    {
        try {
            $this->name = $new;
            $sth = $this->db->prepare('UPDATE `magazyny` SET `nazwa` = ? WHERE `id` = ?');
            if(!$result = $sth->execute(Array($this->name, $this->id)))
                throw new \Exception('Update name of warehouse failed');
            return $result;
        } catch (\Throwable $th) {
            echo '<br/>Uncaught Exception: ',  $th->getMessage(), "\n";
        }
    }
    public function changeCode($new)
    {
        try {
            $this->kod = $new;
            $sth = $this->db->prepare('UPDATE `magazyny` SET `kod` = ? WHERE `id` = ?');
            if(!$result = $sth->execute(Array($this->kod, $this->id)))
                throw new \Exception('Changing code of warehouse failed');
            return $result;
        } catch (\Throwable $th) {
            echo '<br/>Uncaught Exception: ',  $th->getMessage(), "\n";
        }
        
    }
    public static function addNew($kod, $name)
    {
        try {
            $db = new Database();
            $sth = $db->prepare('INSERT INTO `magazyny` SET `kod` = ?, `nazwa` = ?');
            if(!$result = $sth->execute(Array($kod, $name)))
                throw new \Exception('Adding new warehouse to database failed');
            return $result;
        } catch (\Throwable $th) {
            echo '<br/>Uncaught Exception: ',  $th->getMessage(), "\n";
        }
    }
    public static function getAllWarehouses()
    {
        $db = new Database();
        $sth = $db->prepare('SELECT * FROM `magazyny`');
        $sth->execute();
        while($row = $sth->fetch())
        {
            $data[] = $row;
        }
        return $data;
    }
}
?>