<?php
namespace Entities\Kernel;
use Entities\Kernel\Database\Database;
class Kernel extends Database {
    public $db;
    public $pagetitle;
    private $controller;
    function __construct() {
        $this->controller = $_GET['controller'] ?? 'Home';
        $this->db = new Database();
        $this->pagetitle = 'Admin Control Panel';
    }
    function __destruct()
    {
        $this->db = null;
    }
    public function redirect($location, $text = NULL) {
        header("Location: ".$location);
        $_SESSION['redirect'] = $text;
    }
    public function createPagination($array, $countForPage)
    {
        if(!isset($_GET['page']))
            $page = 1;
        else
            $page = $_GET['page'];
        $totalPages = ceil(count($array) / $countForPage);
        $offset = ($page-1) * $countForPage;
        $data = array_slice($array, $offset, $countForPage);
        $newArray = array('totalPages' => $totalPages, 'actualPage'=> $page, 'data' => $data);
        return $newArray;
    }
    public function showPagination($data)
    {
        $this->theme('pagination', 'Home', $data);
    }
    public function theme($name, $path=NULL, $data=NULL)
    {
        try {
            if($path == null)
                $template = '../tpl/'.$name.'.tpl.php';
            else
                $template = '../tpl/'.$path.'/'.$name.'.tpl.php';
            if(!\file_exists($template))
            {
                throw new \Exception('Template not found! Path to: '. substr($template, 2));
            }
            else
            {
                include($template);
            }
        } catch (\Exception $e) {
            echo '<br/>Uncaught Exception: ',  $e->getMessage(), "\n";
        }
    }
    public function loadJS($controller, $path = NULL) {
        if($path === NULL)
        {
            
            $dir = $controller.'/js/';
            $tab = glob($dir.'*.js');
            foreach($tab as $directory)
            {
                echo '<script src="'.$directory.'" type="text/javascript"></script>';
            }
        }
        else
            echo '<script src="'.$path.'"></script>';
        
    }
    
    public function loadCSS($controller, $path = NULL) {
        if($path == NULL)
        {
            $dir = ''.$controller.'/css/';
            $tab = glob($dir.'*.css');
            foreach($tab as $directory)
            {
                echo '<link rel="stylesheet" href="'.$directory.'">';
            }
        }
        else
            echo '<link rel="stylesheet" href="'.$path.'">';
        
    }
}



?>
