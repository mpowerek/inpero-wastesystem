<?php
// =================================
//       Struktura config.ini
// =================================
// [database_access]
// host = 'localhost'
// user = 'root'
// password = ''
// dbname = 'testowa'
// =================================
namespace Entities\Kernel\Database;
use \PDO;
class Database {
    public $connection;
    function __construct() {
        $this->data = parse_ini_file("config.ini");
        try {
            $this->connection = new PDO('mysql:host='.$this->data['host'].';dbname='.$this->data['dbname'], $this->data['user'], $this->data['password'], array(
                PDO::ATTR_PERSISTENT => true
            ));
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function query($sql) {
        return $this->connection->query($sql);
    }
    public function prepare($sql) {
        return $this->connection->prepare($sql);
    }
    public function execute($sql) {
        try {
            $this->connection->execute($sql);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
        }
        
    }
}

?>