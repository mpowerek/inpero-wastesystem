<?php
namespace Magazyny;
use Entities\Warehouse\Warehouse;
class Magazyny {
    function __construct() {

    }
    function Main() {
        global $kernel;
        $kernel->theme('header', 'Home');
        $kernel->theme('body', 'Home');
        // warehouse
        $warehouses = Warehouse::getAllWarehouses();

        $data['warehousesList'] = $kernel->createPagination($warehouses, '10');


        $kernel->theme('showList', 'Magazyny', $data);
        $kernel->theme('footer', 'Home');
    }
    function add()
    {
        global $kernel;
        if(isset($_POST['send']))
        {
            $kod = $_POST['kod'];
            $name = $_POST['name'];
            Warehouse::addNew($kod, $name);
            $kernel->redirect('index.php?controller=Magazyny', 'Dodałeś nowy magazyn o nazwie: '.$name.'');
        }
        else
        {
            $kernel->theme('header', 'Home');
            $kernel->theme('body', 'Home');
            $kernel->theme('addNew', 'Magazyny');
            $kernel->theme('footer', 'Home');
        }
    }
    function Remove()
    {
        global $kernel;
        if(isset($_GET['id']))
            $id = $_GET['id'];
        else
            $kernel->redirect('index.php?controller=Magazyny', 'Nie udało się usunać magazynu!');
        
        $warehouse = new Warehouse();
        $warehouse->remove(array($_GET['id']));
    }
    function Details()
    {
        global $kernel;
        if(isset($_GET['id']))
            $id = $_GET['id'];
        else
            $kernel->redirect('index.php?controller=Magazyny', 'Nie udało się wczytać detali!');
        if(isset($_POST['send']))
        {
            $warehouse->changeName($_POST['name']);
            $warehouse->changeCode($_POST['kod']);
            $kernel->redirect('index.php?controller=Magazyny', 'Zmiany zostały zapisane');
        }
        else
        {
            $warehouse = new Warehouse($_GET['id']);
            $data = array();
            $data['name'] = $warehouse->name;
            $data['kod'] = $warehouse->kod;
    
            $kernel->theme('header', 'Home');
            $kernel->theme('body', 'Home');
            $kernel->theme('showDetails', 'Magazyny', $data);
            $kernel->theme('footer', 'Home');
        }
    }
}
?>